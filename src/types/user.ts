export interface ArgUser {
    name: string;
    lastName: string;
    pass: string;
    email: string;
    create?: Date;
}
