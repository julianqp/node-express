import { Request, Response } from 'express';
import { MongoError } from 'mongodb';
import { ObjectId } from 'mongoose';
import bcrypt from 'bcrypt';
import UserModel, { User } from '../models/user';
import { ArgUser } from '../types/user';
import logger from '../config/logger';

const createUser = async (req: Request, res: Response): Promise<void> => {
    const userInfo = req.body as ArgUser;
    try {
        /* eslint-disable @typescript-eslint/no-floating-promises */
        const hasPass: string = await bcrypt.hash(userInfo.pass, 10);
        logger.info(`hasPass: ${hasPass}`, { service: 'PASS' });

        if (!hasPass) {
            res.json({
                status: 'KO',
                message: 'Error generating hash password',
                data: null,
            });
        } else {
            userInfo.pass = hasPass;
            const expectedNewUser: User = await UserModel.create(userInfo);
            const newUser = await expectedNewUser.save();

            logger.info(`User created: ${String(newUser._id as ObjectId)}`, { service: 'CREATE_USER' });
            res.json({
                status: 'OK',
                message: 'Videogame added successfully!!!',
                data: newUser,
            });
        }
    } catch (err) {
        const error = err as MongoError;
        logger.error(`ERROR: ${error.message}`, { service: 'CREATE_USER' });
        if (error.code && error.code === 11000) {
            res.json({
                status: 'KO',
                message: 'Email already exists',
                data: error,
            });
        } else {
            res.json({
                status: 'KO',
                message: error.message,
                data: error,
            });
        }
    }
};

export default { createUser };
