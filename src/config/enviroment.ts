import { config, DotenvLoadOutput, DotenvConfigOptions } from 'dotenv-flow';

const options: DotenvConfigOptions = {
  node_env: process.env.NODE_ENV,
  default_node_env: 'development',
};

const parsingResult: DotenvLoadOutput = config(options);

process.env = new Proxy(
  { ...process.env },
  {
    set: (): boolean => {
      throw new Error('Cannot modify process.env after loading .env* files');
    },
  },
);

export default parsingResult;
