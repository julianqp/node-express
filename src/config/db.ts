import mongoose, { ConnectionOptions } from 'mongoose';

import logger from './logger';

const nodeEnvironment = process.env.NODE_ENV || 'development';
const mongoUrl = process.env.MONGO_URL || 'localhost';

mongoose.Promise = global.Promise;

mongoose.set('debug', nodeEnvironment === 'development');
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

const options: ConnectionOptions = {
  dbName: process.env.MONGO_DATABASE,
  user: process.env.MONGO_USER,
  pass: process.env.MONGO_PASSWORD,
};

mongoose.connect(mongoUrl, options).catch(async () => {
  await mongoose.createConnection(mongoUrl, options);
});

mongoose.connection
  .once('open', () => {
    logger.info(`Connected to MongoDB on ${mongoUrl}`, { service: 'MONGODB' });
  })
  .on('error', (error: Error) => {
    logger.error(`Error connecting to MongoDB: ${error.message}`, {
      service: 'MONGODB',
    });
    throw error;
  });
