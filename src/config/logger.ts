import winston, { format } from 'winston';
import 'winston-daily-rotate-file';
import environmentLoadingResults from './enviroment';

const nodeEnvironment = process.env.NODE_ENV || 'development';

const defaultOptions = {
    datePattern: 'YYYY-MM-DD',
    dirname: `logs/${nodeEnvironment}`,
    format: format.combine(format.splat(), format.timestamp(), format.ms(), format.json()),
    frequency: '1d',
    maxFiles: nodeEnvironment === 'production' ? '30d' : '7d',
    zippedArchive: true,
};

const logger = winston.createLogger({
    levels: winston.config.syslog.levels,
    transports: [
        // The idea is to change this for a Real Time notification (SMS, Telegram, Discord...)
        new winston.transports.DailyRotateFile({
            ...defaultOptions,
            filename: 'emergency-%DATE%.log',
            level: 'emerg',
        }),
        new winston.transports.DailyRotateFile({
            ...defaultOptions,
            filename: 'error-%DATE%.log',
            level: 'error',
        }),
        new winston.transports.DailyRotateFile({
            ...defaultOptions,
            filename: 'info-%DATE%.log',
            level: 'info',
        }),
    ],
});

if (nodeEnvironment !== 'production') {
    logger.add(
        new winston.transports.Console({
            format: winston.format.combine(winston.format.simple(), winston.format.colorize()),
        })
    );
}

const { error } = environmentLoadingResults;
if (error) {
    logger.crit('Error loading environment variables', { service: 'DOTENV' });
    throw error;
}

export default logger;
