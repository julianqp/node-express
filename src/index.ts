import { AddressInfo } from 'net';
import cors from 'cors';
import express from 'express';
import * as bodyParser from 'body-parser';
import logger from './config/logger';
import './config/enviroment';
import './config/db';
import users from './routes/user';

const app = express();

app.use(cors());
app.use(bodyParser.json());

app.use('/users', users);

app.get('/', (_, res) => res.send('Hello World!'));

const server = app.listen(5000, '0.0.0.0', () => {
    const { port, address } = server.address() as AddressInfo;
    logger.info(`🚀 Server ready at ${address}:${port}`, { service: 'START API' });
});

export default server;
