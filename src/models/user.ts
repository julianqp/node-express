import mongoose, { Document, Schema } from 'mongoose';

export interface User extends Document<Schema.Types.ObjectId> {
    name: string;
    lastName: string;
    pass: string;
    email: string;
    create: Date;
}

const userSchema: Schema<User> = new Schema<User>({
    name: { type: String, required: true, trim: true },
    lastName: { type: String, required: true, trim: true },
    pass: { type: String, required: true, trim: true },
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true,
    },
    create: { type: Date, required: true, default: new Date() },
});

export default mongoose.model<User>('user', userSchema);
