module.exports = {
    clearMocks: true,
    coverageProvider: 'v8',
    coverageReporters: ['html', 'text'],
    moduleFileExtensions: ['ts', 'js'],
    preset: 'ts-jest',
    roots: ['<rootDir>'],
    testEnvironment: 'node',
    transform: {
        '^.+\\.ts?$': 'ts-jest',
    },
    testPathIgnorePatterns: ['/dist/'],
};
