import request from 'supertest';
import app from '../src/index';

describe('Test correcto', () => {
    test('Register OK', async () => {
        const res = await request(app).post('/users').send({
            name: 'Isabel',
            lastName: 'Alcántara de Nicolas',
            email: 'isa.is1a.alcasantara@gmail.com',
            pass: 'Prueba',
        });
        expect(res.status).toEqual(200);
        expect(res.body).toHaveProperty('data');
    });
});
